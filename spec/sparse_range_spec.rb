require 'extrarange'
require 'spec_helper'
require 'date'

include ExtraRange

describe "Range With Gaps" do
  it "returns all elements on each()" do
    r = SparseRange.new(1..3, 5...7)

    collector = []
    r.each { |x| collector.push(x) }

    collector.should == [1, 2, 3, 5, 6]
  end

  it "returns the beginning on begin" do
    r = SparseRange.new(1..3, 5...7)


    r.begin.should == 1
  end

  it "returns the end" do
    r = SparseRange.new(1..3, 5...7)


    r.end.should == 7
  end

  it "returns the exclude_end?" do
    r = SparseRange.new(1..3, 5...7)


    r.exclude_end?.should == true
  end

  it "can check when range doesn't cover element" do
    r = SparseRange.new(1.0..3.0, 5.0...7.0)


    r.cover?(4.0).should == false
  end

  it "can check when range covers element" do
    r = SparseRange.new(1.0..3.0, 5.0...7.0)


    r.cover?(2.5).should == true
  end

  it "can return max element in range" do
    r = SparseRange.new(1..3, 5..10)
    r.max.should == 10
  end

  it "can return max element in range with custom block" do
    r = SparseRange.new(1..3, 5..10)
    r.max{ |a,b| b <=> a}.should == 1
  end

  it "can return min element in range with custom block" do
    r = SparseRange.new(1..3, 5..10)
    r.min{ |a,b| b <=> a}.should == 10
  end

  it "can return min element in range" do
    r = SparseRange.new(1..3, 5..10)
    r.min.should == 1
  end

  it "returns nil on max/min when range contains non-succable items" do
    r = SparseRange.new(3..1, 5..10)
    r.max.should == nil
    r.min.should == nil
  end

  it "can step through elements" do
    r = SparseRange.new(1..3, 5..10)
    r.step(3).should == [1,5,8]
  end

  it "can step through elements with blocks" do
    r = SparseRange.new(1..3, 5..10)
    a = []
    r.step(3) { |x| a.push(x)}
    a.should == [1,5,8]
  end

  it "hashes the same on identical ranges" do
    r1 = SparseRange.new(1..10)
    r2 = SparseRange.new(1..10)

    r1.hash.should == r2.hash
  end

  it "hashes differently on different ranges" do
    r1 = SparseRange.new(1..16)
    r2 = SparseRange.new(1..10)

    r1.hash.should_not == r2.hash
  end

  it "returns all elements on to_a()" do
    r = SparseRange.new(1..3, 5...7)
    r.to_a.should == [1, 2, 3, 5, 6]
  end

  it "combines multiple ranges" do
    r = SparseRange.new(3..19, 90..114, 7..40, 3000..3000, 50...77)
    r.should == SparseRange.new(3..40, 50...77, 90..114, 3000..3000)
  end

  it "subtracts a simple range" do
    r = SparseRange.new(3..5)
    r.delete(3..4)

    r.should == SparseRange.new(5..5)
  end

  it "subtracts complex ranges" do
    r = SparseRange.new(3..19, 90..114, 7..40, 3000..3000, 50...77)
    r2 = SparseRange.new(3..3000)

    r2.delete r
    r2.should == SparseRange.new(41...50, 77...90, 115...3000)
  end

  it "gives the size of a range set" do
    r = SparseRange.new(3..19, 40...43, 50..50, 56...57, 99...99)
    r.size.should == 22
  end

  it "works with dates" do
    r = SparseRange.new((Date.today - 300)..Date.today)
    r.add((Date.today + 40)..(Date.today + 100))

    r.include?(Date.today - 20).should be_true
    r.include?(Date.today + 20).should be_false
  end

  it "works with floats" do
    r = SparseRange.new(12.4...14.1, 33.0..53.9)

    r.delete(35.2..99.9)
    r.include?(35.2).should be_false
    r.include?(35.1999).should be_true

    r.delete(14.1)
    r.include?(14.1).should be_false

    r << (14.1..33.0)
    r.include?(14.1).should be_true
    r.should == SparseRange.new(12.4...14.1, 14.1...35.2)
  end

  it "dups" do
    r = SparseRange.new 1..4
    r2 = SparseRange.new 6..15
    r3 = r + r2

    r.include?(7).should be_false
    r2.include?(3).should be_false
  end

  it "intersects" do
    r = SparseRange.new 1..5, 8..18
    r2 = SparseRange.new 2...5, 9..3000, 3002...400000

    r3 = r & r2
    r3.should == SparseRange.new(2...5, 9..18)
  end

  it "non-overlapping" do
    r = SparseRange.new 1..5
    r2 = SparseRange.new 10...20

    r3 = r | r2
    r3.should == SparseRange.new(1..5, 10...20)
  end


  it "converts single values and enums" do
    r = SparseRange.new 1..5, 8..18

    r.add 6
    r.should == SparseRange.new(1..6, 8..18)

    r.add 200
    r.should == SparseRange.new(1..6, 8..18, 200..200)

    r.add [40, 41, 42, 44, 55..70]
    r.should == SparseRange.new(1..6, 8..18, 40..42, 44..44, 55..70, 200..200)

    r << (20...22)
    r << 22
    r.should == SparseRange.new(1..6, 8..18, 20..22, 40..42, 44..44, 55..70, 200..200)
  end
end
