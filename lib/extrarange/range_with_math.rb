require 'extrarange/range_math'

module ExtraRange
    class RangeWithMath < Range
      include ExtraRange::RangeMath

      def self.from_range(range)
        new range.begin, range.end, range.exclude_end?
      end

      def to_range
        Range.new self.begin, self.end, exclude_end?
      end
    end

end
