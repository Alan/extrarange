require 'extrarange/range_math'
require 'extrarange/range_with_math'

module ExtraRange
  class SparseRange
    include ExtraRange::RangeMath
    include Enumerable

    def initialize(*ranges)
      @ranges = []
      ranges.each { |r| self.add r }
    end

    def begin
      @ranges.first().begin
    end

    alias first begin

    def end
      @ranges.last().end
    end

    alias last end

    def exclude_end?
      @ranges.last().exclude_end?
    end

    def dup
      self.class.new *to_a
    end

    def ==(rset)
      @ranges == rset.instance_variable_get(:@ranges)
    end

    def add(o)
      case o
        when Range then
          _add(o)
        when self.class then
          o.each_range { |b| add b }
        when Enumerable then
          o.each { |b| add b }
        else
          add(o..o)
      end

      return self
    end

    alias << add

    def delete(o)
      case o
        when Range then
          _delete(o)
        when self.class then
          o.each_range { |s| delete s }
        when Enumerable then
          o.each { |s| delete s }
        else
          delete(o..o)
      end

      return self
    end

    def |(enum)
      dup.add(enum)
    end

    alias + |
    alias union |

    def -(enum)
      dup.delete(enum)
    end

    alias difference -

    def &(o)
      ret = self.class.new

      case o
        when Range
          @ranges.each do |r|
            intersection = r & o
            ret.add intersection if intersection
          end
          ret
        when self.class
          o.each_range do |i|
            intersection = self & i
            ret.add intersection if intersection
          end
          ret
        when Enumerable
          o.each do |i|
            intersection = self & i
            ret.add intersection if intersection
          end
          ret
        else
          self&(o..o)
      end
    end

    alias intersection &

    # BEWARE! SparseRange is mutable!
    # it would be better to change its behaviour to return new sparseranges or to
    # create a separate ImmutableSparseRange class.
    def hash
      return @ranges.hash
    end

    def max(&block)
      maxes = @ranges.collect {|r| r.max(&block)}
      (maxes.include? nil) ? nil : maxes.max(&block)
    end

    def min(&block)
      mins = @ranges.collect {|r| r.min(&block)}
      (mins.include? nil) ? nil : mins.min(&block)
    end

    def include?(elem)
      @ranges.any? { |r| r.include? elem }
    end

    alias member? include?

    # currently unoptimized yet working version.
    # TODO: optimize this method to use the builtin cover? method for ranges if
    # they're available, i.e. we're running on Ruby 1.9.1+
    alias cover? include?

    def to_a
      @ranges.map { |r| r.to_a }.flatten
    end

    def each
      to_a.each{ |o| yield o }
    end

    def size
      @ranges.inject(0) { |sum, n| sum + n.size }
    end

    def empty?
      @ranges.empty?
    end

    def each_range
      @ranges.map { |r| r.to_range }.each { |o| yield o }
    end

    def step(n=1, &block)
      enumerator = to_a.select.with_index.select { |_, index| (index % n) == 0 }.collect { |x, _| x }
      if block_given?
        enumerator.each(&block)
      else
        return enumerator
      end
    end

    private

    def _add(new_range)
      new_range = ExtraRange::RangeWithMath.from_range new_range
      return if new_range.empty?

      inserted = false

      @ranges.each_with_index do |r, i|
        if r.overlap?(new_range) || r.adjacent?(new_range)
          new_range = new_range | r
          @ranges[i] = nil
        elsif r.first > new_range.first
          @ranges.insert(i, new_range)
          inserted = true
          break
        end
      end

      @ranges << new_range unless inserted
      @ranges.compact!
    end

    def _delete(o)
      @ranges.map! { |r| r - ExtraRange::RangeWithMath.from_range(o) }
      @ranges.flatten!
      @ranges.compact!
    end
  end
end
