# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = %q{extrarange}
  s.version = "1.0.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 1.3.5") if s.respond_to? :required_rubygems_version=
  s.authors = ["Alan Franzoni"]
  s.date = %q{2010-02-18}
  s.description = %q{The SparseRange class lets you easily collect many ranges into one.You can perform logic operations such as union and intersection on ranges with gaps. Can replace a standard range.}
  s.email = ["username@franzoni.eu"]
  s.files = ["lib/extrarange.rb", "lib/extrarange/range_with_math.rb", "lib/extrarange/range_math.rb", "lib/extrarange/sparse_range.rb", "lib/extrarange/core_ext/range.rb", "LICENSE", "README.markdown"]
  s.homepage = %q{http://extrarange.franzoni.eu}
  s.require_paths = ["lib"]
  s.rubygems_version = %q{1.3.5}
  s.summary = %q{Extended Range functionality}

  if s.respond_to? :specification_version then
    current_version = Gem::Specification::CURRENT_SPECIFICATION_VERSION
    s.specification_version = 3

    if Gem::Version.new(Gem::RubyGemsVersion) >= Gem::Version.new('1.2.0') then
    else
    end
  else
  end
end
